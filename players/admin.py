from django.contrib import admin

from .models import Player, Team, Match, PlayerMatchResult, Tournament

admin.site.register(Player)
admin.site.register(Team)
admin.site.register(Match)
admin.site.register(PlayerMatchResult)
admin.site.register(Tournament)

# Register your models here.
