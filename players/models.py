from django.db import models
from django.db.models import Count, Min, Sum, Avg

class Tournament(models.Model):
	name = models.CharField(max_length=1024)
	url = models.CharField(max_length=1024)
	date = models.DateField()
	next_tour = models.IntegerField(null=True)
	
class Player(models.Model):
	fantasy_id = models.IntegerField(default=0)
	tournament = models.ForeignKey('Tournament')
	team = models.ForeignKey('Team')
	amplua_choice = ((1, 'Вратарь'),
					(2, 'Защитник'),
					(4, 'Нападающий'),
					(3, 'Полузащитник'),)
	amplua = models.IntegerField(max_length=1, choices=amplua_choice, null=True, default=1)
	name = models.CharField(max_length=50)
	price = models.DecimalField(max_digits=3, decimal_places=1, null=True)
	points = models.IntegerField(max_length=3, null=True)
	userchoosed = models.FloatField(null=True)
	url = models.CharField(max_length=1024)
	url_img = models.CharField(max_length=1024)
	isstar=models.BooleanField(default=False)
	goals = models.IntegerField(default=0)
	passes = models.IntegerField(default=0)
	clean_sheets=models.IntegerField(default=0)
	minutes=models.IntegerField(default=0)

class Team(models.Model):
	tournament = models.ForeignKey('Tournament')
	fantasy_id = models.IntegerField(default=0)
	name = models.CharField(max_length=50)
	url = models.CharField(max_length=1024)
	url_img = models.CharField(max_length=1024, null=True)
	place = models.IntegerField(max_length = 2, null=True)
	points = models.IntegerField(max_length=2, null=True)
	fantasypoints = models.IntegerField(null=True)
	win = models.IntegerField(max_length=2, null=True)
	lose = models.IntegerField(max_length=2, null=True)
	drawn = models.IntegerField(max_length=2, null=True)
	score_goals = models.IntegerField(max_length=3, null=True)
	lose_goals = models.IntegerField(max_length=3, null=True)
	country_choices = (('RU', 'Russia'))

	def goalkeeper_points(self):
		return PlayerMatchResult.objects.filter(player__team=self,player__amplua=1).aggregate(line_points=Sum('points'))['line_points']

	def defs_points(self):
		return PlayerMatchResult.objects.filter(player__team=self,player__amplua=2).aggregate(line_points=Sum('points'))['line_points']

	def mids_points(self):
		return PlayerMatchResult.objects.filter(player__team=self,player__amplua=3).aggregate(line_points=Sum('points'))['line_points']

	def forwards_points(self):
		return PlayerMatchResult.objects.filter(player__team=self,player__amplua=4).aggregate(line_points=Sum('points'))['line_points']


class Match(models.Model):
	tournament = models.ForeignKey('Tournament')
	home_team = models.ForeignKey('Team', related_name = 'home', default=0)
	visit_team = models.ForeignKey('Team', related_name='visit',default=0)
	home_team_goals = models.IntegerField(max_length = 2, null=True)
	visit_team_goals = models.IntegerField(max_length=2, null=True)
	url = models.CharField(max_length=1024)
	date = models.DateField()
	tour = models.IntegerField(null=True)
	visitors = models.IntegerField(null=True)

	def full_name(self):

		if self.home_team_goals==None:
			home_team_goals="-"
		else:
			home_team_goals = self.home_team_goals
		if self.visit_team_goals==None:
			visit_team_goals = "-"
		else:
			visit_team_goals = self.visit_team_goals

		return "{0} {1} : {2} {3}".format(self.home_team.name, 
			home_team_goals, visit_team_goals,
			self.visit_team.name)
	
	def home_team_points(self):
		return PlayerMatchResult.objects.filter(match = self, player__team=self.home_team).aggregate(match_points=Sum('points'))['match_points']

	def visit_team_points(self):
		return PlayerMatchResult.objects.filter(match = self, player__team=self.visit_team).aggregate(match_points=Sum('points'))['match_points']


class PlayerMatchResult(models.Model):
	match = models.ForeignKey('Match')
	player = models.ForeignKey('Player', default=0)
	minute = models.IntegerField(max_length = 3)
	goals = models.IntegerField(max_length=2)
	passes = models.IntegerField(max_length = 2)
	lose_penalty = models.IntegerField(max_length = 2)
	yellow_cards = models.IntegerField(max_length=2)
	red_cards = models.IntegerField(max_length=2)
	clean_sheet = models.BooleanField()
	points = models.IntegerField(max_length=2)
