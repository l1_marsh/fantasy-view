from django.shortcuts import render
from django.http import HttpResponse
from django.template import RequestContext, loader
from django.db.models import Count, Min, Sum, Avg, Q
import datetime

from .models import Player, Match, Team, PlayerMatchResult, Tournament

def home(request):
	
	tournament_list = Tournament.objects.all()
	template = loader.get_template('index.html')
	context = RequestContext(request, {'tournament_list':tournament_list, })
	return HttpResponse(template.render(context))
	
def tournament_home(request, tournament_id):
	tournament = Tournament.objects.get(id = tournament_id)
	points_count = PlayerMatchResult.objects.filter(match__tournament_id=tournament_id).aggregate(total_points = Sum('points'))['total_points']
	match_list = Match.objects.filter(tournament_id=tournament_id, tour__lte=tournament.next_tour).order_by('date')
	players_list = Player.objects.filter(tournament=tournament).exclude(points=None).order_by('-points')[:3]
	team_list = Team.objects.filter(tournament=tournament).order_by('-fantasypoints')[:3]
	template = loader.get_template('home_tournament.html')
	context = RequestContext(request, {'players_count': Player.objects.filter(tournament=tournament).count(),
	 'match_count': match_list.count(), 'team_count': Team.objects.filter(tournament=tournament).count(),
	  'points_count': points_count, 'tournament': tournament, 'match_list':match_list, 'players_list':players_list, 'team_list':team_list})
	return HttpResponse(template.render(context))

def teams(request, tournament_id):
	#points_count = PlayerMatchResult.objects.aggregate(total_points = Sum('points'))['total_points']
	tournament = Tournament.objects.get(id = tournament_id)
	match_list = Match.objects.filter(tournament_id=tournament_id, tour__lte=tournament.next_tour-1).order_by('date')
	team_list = Team.objects.filter(tournament_id = tournament_id)
	for team in team_list:
		team.result_list=[]
		for match in match_list:
			if match.home_team.id == team.id:
				team.result_list = team.result_list + [match.home_team_points()]
			if match.visit_team.id == team.id:
				team.result_list = team.result_list + [match.visit_team_points()]

		#print (team.result_list)
		#team.result_list = PlayerMatchResult.objects.filter(player__team=team).annotate
	all_points = PlayerMatchResult.objects.filter(match__tournament_id=tournament_id).aggregate(total_points = Sum('points'))['total_points']

	for team in team_list:
		team.players_points = int(PlayerMatchResult.objects.filter(match__tournament_id=tournament_id, player__team=team).aggregate(team_players_points=Sum('points'))['team_players_points'])
		team.progress = (team.players_points/all_points)*100


	team_list = sorted(team_list, key=lambda team:team.players_points, reverse=True)
	#print (type(team_list))

	#team_list.sort(key=team.players_points)
	template = loader.get_template('teams.html')
	context = RequestContext(request, {'team_list': team_list, 'tournament': tournament, 'match_list':match_list })

	return HttpResponse(template.render(context))

def matches(request, tournament_id):
	tournament = Tournament.objects.get(id = tournament_id)
	match_list = Match.objects.filter(tournament_id=tournament_id).order_by('date')
	for match in match_list:
		if match.tour<tournament.next_tour:
			match.home_team_points = match.home_team_points()#PlayerMatchResult.objects.filter(match = match, player__team=match.home_team).aggregate(match_points=Sum('points'))['match_points']
			match.visit_team_points = match.visit_team_points()#PlayerMatchResult.objects.filter(match = match, player__team=match.visit_team).aggregate(match_points=Sum('points'))['match_points']
			
	template = loader.get_template('matches.html')

	context = RequestContext(request, {'match_list': match_list, 'tournament':tournament})

	return HttpResponse(template.render(context))

def match_detail(request, tournament_id, match_id):
	
	tournament = Tournament.objects.get(id=tournament_id)

	match = Match.objects.get(id=match_id)

	home_results = PlayerMatchResult.objects.filter(match=match, player__team=match.home_team).order_by('-points')
	visit_results = PlayerMatchResult.objects.filter(match=match, player__team=match.visit_team).order_by('-points')
	
	template = loader.get_template('match.html')
	context = RequestContext(request, {'tournament':tournament, 'match': match, 'home_results':home_results, 'visit_results':visit_results})

	return HttpResponse(template.render(context))

def players(request, tournament_id):

	tournament = Tournament.objects.get(id = tournament_id)
	#points_count = PlayerMatchResult.objects.aggregate(total_points = Sum('points'))['total_points']
	players_list = Player.objects.filter(tournament_id = tournament_id).exclude(points=None).order_by('-points')

	for player in players_list:
		player.pmr = []
		pr = PlayerMatchResult.objects.filter(match__tournament_id=tournament_id).filter(player=player).filter(match__tour__lte=tournament.next_tour).order_by('match__tour')[:5]
		for p in pr:
			player.pmr.append(p.points)

	
	template = loader.get_template('players.html')
	context = RequestContext(request, {'players_list': players_list, 'tournament':tournament})


	return HttpResponse(template.render(context))

def player_detail(request, player_id, tournament_id):
	tournament = Tournament.objects.get(id=tournament_id)
	#points_count = PlayerMatchResult.objects.aggregate(total_points = Sum('points'))['total_points']
	player = Player.objects.get(id=player_id)
	pmr_list = PlayerMatchResult.objects.filter(player = player).order_by('match__tour')[:10]
	template = loader.get_template('player.html')
	context = RequestContext(request, {'player': player, 'pmr_list':pmr_list, 'tournament':tournament})

	return HttpResponse(template.render(context))

def team_detail(request, team_id, tournament_id):
	tournament = Tournament.objects.get(id=tournament_id)
	team = Team.objects.get(id=team_id, tournament = tournament)
	match_list = Match.objects.filter(tournament=tournament).filter(Q(visit_team=team)|Q(home_team=team)).exclude(tour__gte=tournament.next_tour).order_by('tour')[:10]
	
	team.match_list=match_list
	team.pmr = []
	for team.match in team.match_list:
		points = PlayerMatchResult.objects.filter(player__team = team, match=team.match).exclude(points=None).aggregate(tour_points=Sum('points'))['tour_points']
		team.match.points=points

	players_list = Player.objects.filter(tournament_id = tournament_id, team=team).exclude(points=None).order_by('-points')

	for player in players_list:
		player.pmr = []
		pr = PlayerMatchResult.objects.filter(match__tournament_id=tournament_id).filter(player=player).filter(match__tour__lte=tournament.next_tour).order_by('match__tour')[:5]
		for p in pr:
			player.pmr.append(p.points)

	template = loader.get_template('team.html')
	context = RequestContext(request, {'tournament':tournament, 'team': team, 'tour_results':team.match_list, 'players_list':players_list})

	return HttpResponse(template.render(context))

def pmr(request, tournament_id):

	tournament = Tournament.objects.get(id=tournament_id)

	players_list = Player.objects.filter(tournament=tournament).exclude(points=None).order_by('-points')[:100]

	template = loader.get_template('pmr.html')
	context = RequestContext(request, {'tournament': tournament, 'players_list':players_list})

	return HttpResponse(template.render(context))
