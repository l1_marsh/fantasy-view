from ..models import Player, Team, Match, PlayerMatchResult, Tournament
from django import template

register = template.Library()

@register.inclusion_tag('tournament_list.html')
def tournament_list():
	tournament_list = Tournament.objects.all()
	return {'tournament_list':tournament_list}