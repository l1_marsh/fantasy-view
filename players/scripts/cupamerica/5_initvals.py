import django
import os
import sys
import json
import urllib.request
import http.client
from bs4 import BeautifulSoup
import logging
sys.path.append(os.path.join('/home/web/', 'fantasyIn'))
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "fantasyIn.settings")
from django.conf import settings
from players.models import Player, Team, Match, PlayerMatchResult, Tournament
from django.db.models import Count, Min, Sum, Avg, Q

django.setup()

if  __name__ ==  "__main__" :
	tournament =  Tournament.objects.get(name = "Кубок Америки")
	teams =  Team.objects.filter(tournament=tournament)
	for team in teams:
		team.drawn=0
		team.score_goals = 0
		team.lose_goals = 0
		team.win = 0
		team.lose = 0

		try:
			team.fantasypoints =  int(PlayerMatchResult.objects.filter(match__tournament=tournament, player__team=team).aggregate(team_players_points=Sum('points'))['team_players_points'])
			team_matches = Match.objects.filter(tournament=tournament).filter(Q(visit_team=team)|Q(home_team=team)).exclude(visit_team_goals=None)
			for match in team_matches:
				if match.home_team_goals == match.visit_team_goals:
					team.drawn = team.drawn + 1
					team.score_goals =team.score_goals + match.home_team_goals
					team.lose_goals = team.lose_goals + match.visit_team_goals

				else:
					if match.home_team_goals > match.visit_team_goals and match.home_team.name == team.name:
						team.win = team.win + 1
						team.score_goals =team.score_goals + match.home_team_goals
						team.lose_goals = team.lose_goals + match.visit_team_goals
					elif match.home_team_goals > match.visit_team_goals and match.home_team.name != team.name:
						team.lose = team.lose + 1
						team.score_goals =team.score_goals + match.visit_team_goals
						team.lose_goals = team.lose_goals + match.home_team_goals
					elif match.home_team_goals < match.visit_team_goals and match.home_team.name == team.name:
						team.lose = team.lose + 1
						team.score_goals =team.score_goals + match.home_team_goals
						team.lose_goals = team.lose_goals + match.visit_team_goals
					elif match.home_team_goals < match.visit_team_goals and match.home_team.name != team.name:
						team.win = team.win + 1
						team.score_goals =team.score_goals + match.visit_team_goals
						team.lose_goals = team.lose_goals + match.home_team_goals
			team.save()

		except Exception as e:
			print (e)

		

	players = Player.objects.filter(tournament=tournament)
	for player in players:
	# 	try:
	# 		player.goals = int(PlayerMatchResult.objects.filter(player=player).aggregate(goals=Sum('goals'))['goals'])
	# 	except:
	# 		player.goals=0
	# 		print (player.name)
	# 	try:
	# 		player.passes = int(PlayerMatchResult.objects.filter(player=player).aggregate(passes=Sum('passes'))['passes'])
	# 	except:
	# 		player.passes=0
	# 		print (player.name)
	# 	try:
	# 		zr = PlayerMatchResult.objects.filter(match__tournament=tournament).filter(player=player).filter(clean_sheet=True)
	# 		player.clean_sheets =zr.count()
	# 	except:
	# 		player.clean_sheets = 0
	# 		print(player.name)
		try:
			player.minutes = int(PlayerMatchResult.objects.filter(player=player).aggregate(minute=Sum('minute'))['minute'])
		except:
			player.minutes=0
			print (player.name)
		player.save()