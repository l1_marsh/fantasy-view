import django
import os
import sys
import json
import urllib.request
import http.client
from bs4 import BeautifulSoup
import logging
sys.path.append(os.path.join('/home/web/', 'fantasyIn'))
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "fantasyIn.settings")
from django.conf import settings
from players.models import Player, Team, Match, PlayerMatchResult, Tournament
from django.db.models import Count, Min, Sum, Avg

django.setup()

if  __name__ ==  "__main__" :
	tournament =  Tournament.objects.get(name = "Россия. Премьер-лига")
	teams =  Team.objects.filter(tournament=tournament)
	for team in teams:
		try:
			team.fantasypoints =  int(PlayerMatchResult.objects.filter(match__tournament=tournament, player__team=team).aggregate(team_players_points=Sum('points'))['team_players_points'])
		except:
			print (team.name)

		team.save()

	# players = Player.objects.filter(tournament=tournament)
	# for player in players:
	# 	try:
	# 		player.goals = int(PlayerMatchResult.objects.filter(player=player).aggregate(goals=Sum('goals'))['goals'])
	# 	except:
	# 		player.goals=0
	# 		print (player.name)
	# 	try:
	# 		player.passes = int(PlayerMatchResult.objects.filter(player=player).aggregate(passes=Sum('passes'))['passes'])
	# 	except:
	# 		player.passes=0
	# 		print (player.name)
	# 	try:
	# 		zr = PlayerMatchResult.objects.filter(match__tournament=tournament).filter(player=player).filter(clean_sheet=True)
	# 		player.clean_sheets =zr.count()
	# 	except:
	# 		player.clean_sheets = 0
	# 		print(player.name)
	# 	player.save()