import os
import sys
import json
import urllib.request
import http.client
#from bs4 import BeautifulSoup
import logging
sys.path.append(os.path.join('/home/web/', 'fantasyIn'))
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "fantasyIn.settings")
from django.conf import settings
from players.models import Player, Team, Tournament

#url = "http://www.sports.ru/fantasy/football/team/json/1244331.json"
fl = open("log_1.txt", "w")
#Save url
'''url2= "http://www.sports.ru/fantasy/football/team/create/31.json"

try:
	page = urllib.request.Request(url2, headers=HEADERS)
	response = urllib.request.urlopen(page)
except Exception as e:
	print ("get_response:%s", e)
	response = None
'''

with open('31.json') as data_file:    
    data = json.load(data_file)

############################################
####Get Tournament
tournament =  Tournament.objects.get(name = "Россия. Премьер-лига")
############################################

#Download teams
# for tm in data['teams']:
# 	team = Team(fantasy_id = tm["id"], name = tm["name"], tournament = tournament)
# 	team.save()

# print(Team.objects.filter(tournament = tournament).count())
#
#"id":1805998,"amplua":1,"tournament_id":31,
#"club_id":739,"club":"\u0423\u0440\u0430\u043b","is_star":0,
#"is_injured":0,"is_disqual":0,"points":4,"name":"\u0410\u0440\u0430\u043f\u043e\u0432",
#"price":"4.5","img":"http:\/\/s5o.ru\/fantasy\/images\/shirts\/football\/russia\/ural.png",
#"tour_points":"-","sort_surname":"
#\u0410\u0440\u0430\u043f\u043e\u0432"

for pl in data['players']:
	player = Player(team = Team.objects.get(fantasy_id=pl["club_id"]),
		fantasy_id = pl["id"],
		isstar=bool(pl["is_star"]),
		amplua = int(pl["amplua"]),
		points=pl["points"],
		name=pl["name"],
		price = pl["price"],
		tournament = tournament)
	
	try:
		player.save()
	except:
		print("fail")
		fl.write("Fail:{0}\n".format(player.fantasy_id))

print(Player.objects.filter(tournament = tournament).count())





