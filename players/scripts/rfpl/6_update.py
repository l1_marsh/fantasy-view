import django
import os
import sys
import json
import urllib.request
import http.client
from bs4 import BeautifulSoup
import logging
sys.path.append(os.path.join('/home/web/', 'fantasyIn'))
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "fantasyIn.settings")
from django.conf import settings
from players.models import Player, Team, Match, PlayerMatchResult, Tournament
from django.db.models import Count, Min, Sum, Avg, Q

import datetime
import re

django.setup()

def update_match_result(tournament):

	url = "http://www.sports.ru/rfpl/calendar/"

	header = {'User-Agent': 'Mozilla/5.0'} 
	req = urllib.request.Request(url,headers=header)
	page = urllib.request.urlopen(req)
	soup = BeautifulSoup(page)

	tables = soup.findAll("table", { "class" : "stat-table" })
	i=0

	#print(Match.objects.all().count())
	#Match.objects.all().delete()

	for table in tables:
		i=i+1
		for row in table.findAll("tr"):

			cells = row.findAll("td")
			url = row.find("a", { "class" : "score" })
			if cells[2].text != "Счет":
				date = datetime.datetime.strptime(cells[0].text.split('|')[0],"%d.%m.%Y")
				
				#if date < datetime.datetime.strptime("2017-08-08","%Y-%m-%d"):
				if i == tournament.next_tour:
					#home_team_goals,visit_team_goals = cells[2].text.split(':')
					try:
						#print("{0},,,,{1}".format(re.findall('\d+', cells[2].text)))
						#print("{0}".format(cells[2].text))
						home_team_goals,visit_team_goals = re.findall('\d+', cells[2].text)
					except Exception as e:
						print(e)
						home_team_goals = None
						visit_team_goals = None
					#print (home_team_goals,visit_team_goals)
					home_team = Team.objects.get(name=cells[1].text)
					visit_team = Team.objects.get(name=cells[3].text)
					

					try:
						visitors = int(cells[4].text)
					except:
						visitors = 0
					#print(cells[1].text, home_team_goals,visit_team_goals,visit_team.name)
					try:
						m = Match.objects.get(home_team=home_team, visit_team=visit_team)
					except Exception as e:
						print("Ошибка при получении матча{0}".format(e))

					m.home_team_goals = home_team_goals
					m.visit_team_goals = visit_team_goals
					m.url = url['href']
					m.visitors = visitors
					m.date = date
					print (m.full_name())
					m.save()
	return True

def update_pmr(tournament):
	tempurl = "http://www.sports.ru/fantasy/football/player/info/31/{0}.html"
	fl = open("log.txt", "w")

	for player in Player.objects.filter(tournament=tournament):
		url = tempurl.format(player.fantasy_id)
		print (player.name)
		#print (url)
		try:
			header = {'User-Agent': 'Mozilla/5.0'} 
			req = urllib.request.Request(url,headers=header)
			page = urllib.request.urlopen(req)
			soup = BeautifulSoup(page)

			img = soup.find("img", { "height" : "150" })
			player.url_img = img['src']
###########################SAVE##################################
			player.save()
			#print (img['src'])

			tables = soup.findAll("table", { "class" : "profile-table" })



			for table in tables:
				for row in table.findAll("tr"):

					if "Выбрали" in row.text:
						cells = row.find("td")
						player.userchoosed = float(str(cells.text).split('%')[0])
						player.save()


			table = soup.find("table", { "class" : "stat-table" })


			for row in table.findAll("tr"):

				cells = row.findAll("td")
				
				urlmatch = row.find("a", {"class" : "score"})

				err=0

				if urlmatch != None:
					u  = urlmatch["href"]
					

					if cells[10].text == "+":
						clean_sheet = True
						
					else:
						clean_sheet = False

					points = cells[11].text

					if points=="":
						points=0
					else:
						points = int(points)

					try:
						match = Match.objects.get(url__endswith=u[17:])
						err=0
					
					except  Exception as e:
						err=1
						print("cudnt find match: {0}\n{1}".format(u,e))

					print(err)
					if err!=1 and match.tour == tournament.next_tour:
						pmr = PlayerMatchResult(match = match,
							player = player,
							minute = int(cells[4].text),
							goals =  int(cells[5].text),
							passes = int(cells[6].text),
							lose_penalty = int(cells[7].text),
							yellow_cards = int(cells[8].text),
							red_cards = int(cells[9].text),
							clean_sheet = clean_sheet,
							points = points,
							)						

						# pmr = PlayerMatchResult.objects.get(match=match, player=player)

						# pmr.minute = int(cells[4].text)
						# pmr.goals =  int(cells[5].text)
						# pmr.passes = int(cells[6].text)
						# pmr.lose_penalty = int(cells[7].text)
						# pmr.yellow_cards = int(cells[8].text)
						# pmr.red_cards = int(cells[9].text)
						# pmr.clean_sheet = clean_sheet
						# pmr.points = points
						
						print ("Success add player results:{0}, {1}".format(player.name, match.full_name()))	
						
##################################SAAAAAAAAAAAAAAAAAAAAAAVAAEEEEEEEEEEEEEEEEEEE
						pmr.save()
						#print("done")
		except Exception as e:
			print(e)
			fl.write("Fail:{0}, url:{1} \n".format(player.name, url))

	fl.close()

	return True

if  __name__ ==  "__main__" :
	tournament =  Tournament.objects.get(name = "Россия. Премьер-лига")

	#update_match_result(tournament)

	update_pmr(tournament)

	teams =  Team.objects.filter(tournament=tournament)
	for team in teams:
		team.drawn=0
		team.score_goals = 0
		team.lose_goals = 0
		team.win = 0
		team.lose = 0

		try:
			team.fantasypoints =  int(PlayerMatchResult.objects.filter(match__tournament=tournament, player__team=team).aggregate(team_players_points=Sum('points'))['team_players_points'])
			team_matches = Match.objects.filter(tournament=tournament).filter(Q(visit_team=team)|Q(home_team=team)).exclude(visit_team_goals=None)
			for match in team_matches:
				if match.home_team_goals == match.visit_team_goals:
					team.drawn = team.drawn + 1
					team.score_goals =team.score_goals + match.home_team_goals
					team.lose_goals = team.lose_goals + match.visit_team_goals

				else:
					if match.home_team_goals > match.visit_team_goals and match.home_team.name == team.name:
						team.win = team.win + 1
						team.score_goals =team.score_goals + match.home_team_goals
						team.lose_goals = team.lose_goals + match.visit_team_goals
					elif match.home_team_goals > match.visit_team_goals and match.home_team.name != team.name:
						team.lose = team.lose + 1
						team.score_goals =team.score_goals + match.visit_team_goals
						team.lose_goals = team.lose_goals + match.home_team_goals
					elif match.home_team_goals < match.visit_team_goals and match.home_team.name == team.name:
						team.lose = team.lose + 1
						team.score_goals =team.score_goals + match.home_team_goals
						team.lose_goals = team.lose_goals + match.visit_team_goals
					elif match.home_team_goals < match.visit_team_goals and match.home_team.name != team.name:
						team.win = team.win + 1
						team.score_goals =team.score_goals + match.visit_team_goals
						team.lose_goals = team.lose_goals + match.home_team_goals
			team.save()

		except Exception as e:
			print (e)

		

	players = Player.objects.filter(tournament=tournament)
	for player in players:
		try:
			player.goals = int(PlayerMatchResult.objects.filter(player=player).aggregate(goals=Sum('goals'))['goals'])
		except:
			player.goals=0
			print (player.name)
		try:
			player.passes = int(PlayerMatchResult.objects.filter(player=player).aggregate(passes=Sum('passes'))['passes'])
		except:
			player.passes=0
			print (player.name)
		try:
			zr = PlayerMatchResult.objects.filter(match__tournament=tournament).filter(player=player).filter(clean_sheet=True)
			player.clean_sheets =zr.count()
		except:
			player.clean_sheets = 0
			print(player.name)
		try:
			player.minutes = int(PlayerMatchResult.objects.filter(player=player).aggregate(minute=Sum('minute'))['minute'])
		except:
			player.minutes=0
			print (player.name)
############SAAAAAAAAAAAAVVVVVVEEEEEEEE
		player.save()


