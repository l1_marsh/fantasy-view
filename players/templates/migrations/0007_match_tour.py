# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('players', '0006_auto_20150620_1329'),
    ]

    operations = [
        migrations.AddField(
            model_name='match',
            name='tour',
            field=models.IntegerField(null=True),
        ),
    ]
