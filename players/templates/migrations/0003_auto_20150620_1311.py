# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('players', '0002_auto_20150619_1705'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='player',
            name='pub_date',
        ),
        migrations.AddField(
            model_name='player',
            name='isstar',
            field=models.BooleanField(default=False),
        ),
    ]
