# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('players', '0004_auto_20150620_1320'),
    ]

    operations = [
        migrations.AddField(
            model_name='player',
            name='amplua',
            field=models.IntegerField(null=True, max_length=1),
        ),
    ]
