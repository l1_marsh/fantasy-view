# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('players', '0011_auto_20150622_1332'),
    ]

    operations = [
        migrations.AlterField(
            model_name='player',
            name='price',
            field=models.DecimalField(null=True, max_digits=3, decimal_places=1),
        ),
    ]
