# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('players', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='player',
            name='fantasy_id',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='team',
            name='fantasy_id',
            field=models.IntegerField(default=0),
        ),
    ]
