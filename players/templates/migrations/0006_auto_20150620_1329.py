# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('players', '0005_player_amplua'),
    ]

    operations = [
        migrations.AlterField(
            model_name='player',
            name='points',
            field=models.IntegerField(null=True, max_length=3),
        ),
        migrations.AlterField(
            model_name='player',
            name='price',
            field=models.DecimalField(decimal_places=1, null=True, max_digits=2),
        ),
        migrations.AlterField(
            model_name='player',
            name='userchoosed',
            field=models.DecimalField(decimal_places=2, null=True, max_digits=2),
        ),
    ]
