# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('players', '0013_remove_player_amplua'),
    ]

    operations = [
        migrations.CreateModel(
            name='Tournament',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('name', models.CharField(max_length=1024)),
                ('url', models.CharField(max_length=1024)),
                ('date', models.DateField()),
            ],
        ),
        migrations.AddField(
            model_name='player',
            name='amplua',
            field=models.IntegerField(max_length=1, choices=[(1, 'Goalkeeper'), (2, 'Defender'), (4, 'Attaker'), (3, 'Midfielder')], null=True, default=1),
        ),
        migrations.AddField(
            model_name='player',
            name='tournament',
            field=models.ForeignKey(default=0, to='players.Tournament'),
        ),
        migrations.AddField(
            model_name='playermatchresult',
            name='tournament',
            field=models.ForeignKey(default=0, to='players.Tournament'),
        ),
        migrations.AddField(
            model_name='team',
            name='tournament',
            field=models.ForeignKey(default=0, to='players.Tournament'),
        ),
    ]
