# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Match',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('home_team_goals', models.IntegerField(max_length=2)),
                ('visit_team_goals', models.IntegerField(max_length=2)),
                ('url', models.CharField(max_length=1024)),
                ('date', models.DateField()),
            ],
        ),
        migrations.CreateModel(
            name='Player',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('pub_date', models.DateTimeField(verbose_name='date published')),
                ('price', models.DecimalField(decimal_places=1, max_digits=2)),
                ('points', models.IntegerField(max_length=3)),
                ('userchoosed', models.DecimalField(decimal_places=2, max_digits=2)),
                ('url', models.CharField(max_length=1024)),
            ],
        ),
        migrations.CreateModel(
            name='PlayerMatchResult',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('minute', models.IntegerField(max_length=3)),
                ('goals', models.IntegerField(max_length=2)),
                ('passes', models.IntegerField(max_length=2)),
                ('lose_penalty', models.IntegerField(max_length=2)),
                ('yellow_cards', models.IntegerField(max_length=2)),
                ('red_cards', models.IntegerField(max_length=2)),
                ('clean_sheet', models.BooleanField()),
                ('points', models.IntegerField(max_length=2)),
                ('match', models.ForeignKey(to='players.Match')),
            ],
        ),
        migrations.CreateModel(
            name='Team',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('url', models.CharField(max_length=1024)),
                ('place', models.IntegerField(max_length=2)),
                ('points', models.IntegerField(max_length=2)),
                ('win', models.IntegerField(max_length=2)),
                ('lose', models.IntegerField(max_length=2)),
                ('drawn', models.IntegerField(max_length=2)),
                ('score_goals', models.IntegerField(max_length=3)),
                ('lose_goals', models.IntegerField(max_length=3)),
            ],
        ),
        migrations.AddField(
            model_name='player',
            name='team',
            field=models.ForeignKey(to='players.Team'),
        ),
        migrations.AddField(
            model_name='match',
            name='home_team',
            field=models.ManyToManyField(to='players.Team', related_name='match_home'),
        ),
        migrations.AddField(
            model_name='match',
            name='visit_team',
            field=models.ManyToManyField(to='players.Team', related_name='match_visit'),
        ),
    ]
