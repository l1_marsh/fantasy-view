# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('players', '0012_auto_20150715_1259'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='player',
            name='amplua',
        ),
    ]
