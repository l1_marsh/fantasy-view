# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('players', '0010_playermatchresult_player'),
    ]

    operations = [
        migrations.AlterField(
            model_name='player',
            name='userchoosed',
            field=models.FloatField(null=True),
        ),
    ]
