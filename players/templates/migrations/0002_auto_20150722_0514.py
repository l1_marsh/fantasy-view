# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('players', '0001_squashed_0014_auto_20150722_0459'),
    ]

    operations = [
        migrations.AlterField(
            model_name='player',
            name='tournament',
            field=models.ForeignKey(null=True, to='players.Tournament'),
        ),
        migrations.AlterField(
            model_name='playermatchresult',
            name='tournament',
            field=models.ForeignKey(null=True, to='players.Tournament'),
        ),
        migrations.AlterField(
            model_name='team',
            name='tournament',
            field=models.ForeignKey(null=True, to='players.Tournament'),
        ),
    ]
