# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('players', '0003_auto_20150722_0521'),
    ]

    operations = [
        migrations.CreateModel(
            name='Tournament',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, auto_created=True, verbose_name='ID')),
                ('name', models.CharField(max_length=1024)),
                ('url', models.CharField(max_length=1024)),
                ('date', models.DateField()),
            ],
        ),
        migrations.AddField(
            model_name='match',
            name='tournament',
            field=models.ForeignKey(to='players.Tournament', null=True),
        ),
        migrations.AddField(
            model_name='player',
            name='tournament',
            field=models.ForeignKey(to='players.Tournament', null=True),
        ),
        migrations.AddField(
            model_name='team',
            name='tournament',
            field=models.ForeignKey(to='players.Tournament', null=True),
        ),
    ]
