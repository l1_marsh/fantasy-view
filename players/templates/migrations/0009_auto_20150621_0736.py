# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('players', '0008_match_visitors'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='match',
            name='home_team',
        ),
        migrations.AddField(
            model_name='match',
            name='home_team',
            field=models.ForeignKey(to='players.Team', related_name='home', default=0),
        ),
        migrations.RemoveField(
            model_name='match',
            name='visit_team',
        ),
        migrations.AddField(
            model_name='match',
            name='visit_team',
            field=models.ForeignKey(to='players.Team', related_name='visit', default=0),
        ),
    ]
