# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('players', '0003_auto_20150620_1311'),
    ]

    operations = [
        migrations.AlterField(
            model_name='team',
            name='drawn',
            field=models.IntegerField(null=True, max_length=2),
        ),
        migrations.AlterField(
            model_name='team',
            name='lose',
            field=models.IntegerField(null=True, max_length=2),
        ),
        migrations.AlterField(
            model_name='team',
            name='lose_goals',
            field=models.IntegerField(null=True, max_length=3),
        ),
        migrations.AlterField(
            model_name='team',
            name='place',
            field=models.IntegerField(null=True, max_length=2),
        ),
        migrations.AlterField(
            model_name='team',
            name='points',
            field=models.IntegerField(null=True, max_length=2),
        ),
        migrations.AlterField(
            model_name='team',
            name='score_goals',
            field=models.IntegerField(null=True, max_length=3),
        ),
        migrations.AlterField(
            model_name='team',
            name='win',
            field=models.IntegerField(null=True, max_length=2),
        ),
    ]
