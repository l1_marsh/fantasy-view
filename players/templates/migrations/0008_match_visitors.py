# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('players', '0007_match_tour'),
    ]

    operations = [
        migrations.AddField(
            model_name='match',
            name='visitors',
            field=models.IntegerField(null=True),
        ),
    ]
