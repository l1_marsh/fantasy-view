# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('players', '0002_auto_20150722_0514'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='player',
            name='tournament',
        ),
        migrations.RemoveField(
            model_name='playermatchresult',
            name='tournament',
        ),
        migrations.RemoveField(
            model_name='team',
            name='tournament',
        ),
        migrations.DeleteModel(
            name='Tournament',
        ),
    ]
