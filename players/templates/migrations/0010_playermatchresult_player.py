# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('players', '0009_auto_20150621_0736'),
    ]

    operations = [
        migrations.AddField(
            model_name='playermatchresult',
            name='player',
            field=models.ForeignKey(to='players.Player', default=0),
        ),
    ]
