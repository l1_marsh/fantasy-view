# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Match',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('home_team_goals', models.IntegerField(max_length=2)),
                ('visit_team_goals', models.IntegerField(max_length=2)),
                ('url', models.CharField(max_length=1024)),
                ('date', models.DateField()),
                ('tour', models.IntegerField(null=True)),
                ('visitors', models.IntegerField(null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Player',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('fantasy_id', models.IntegerField(default=0)),
                ('amplua', models.IntegerField(max_length=1, null=True, choices=[(1, 'Goalkeeper'), (2, 'Defender'), (4, 'Attaker'), (3, 'Midfielder')], default=1)),
                ('name', models.CharField(max_length=50)),
                ('price', models.DecimalField(null=True, max_digits=3, decimal_places=1)),
                ('points', models.IntegerField(max_length=3, null=True)),
                ('userchoosed', models.FloatField(null=True)),
                ('url', models.CharField(max_length=1024)),
                ('url_img', models.CharField(max_length=1024)),
                ('isstar', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='PlayerMatchResult',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('minute', models.IntegerField(max_length=3)),
                ('goals', models.IntegerField(max_length=2)),
                ('passes', models.IntegerField(max_length=2)),
                ('lose_penalty', models.IntegerField(max_length=2)),
                ('yellow_cards', models.IntegerField(max_length=2)),
                ('red_cards', models.IntegerField(max_length=2)),
                ('clean_sheet', models.BooleanField()),
                ('points', models.IntegerField(max_length=2)),
                ('match', models.ForeignKey(to='players.Match')),
                ('player', models.ForeignKey(to='players.Player', default=0)),
            ],
        ),
        migrations.CreateModel(
            name='Team',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('fantasy_id', models.IntegerField(default=0)),
                ('name', models.CharField(max_length=50)),
                ('url', models.CharField(max_length=1024)),
                ('place', models.IntegerField(max_length=2, null=True)),
                ('points', models.IntegerField(max_length=2, null=True)),
                ('win', models.IntegerField(max_length=2, null=True)),
                ('lose', models.IntegerField(max_length=2, null=True)),
                ('drawn', models.IntegerField(max_length=2, null=True)),
                ('score_goals', models.IntegerField(max_length=3, null=True)),
                ('lose_goals', models.IntegerField(max_length=3, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Tournament',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('name', models.CharField(max_length=1024)),
                ('url', models.CharField(max_length=1024)),
                ('date', models.DateField()),
            ],
        ),
        migrations.AddField(
            model_name='team',
            name='tournament',
            field=models.ForeignKey(to='players.Tournament'),
        ),
        migrations.AddField(
            model_name='player',
            name='team',
            field=models.ForeignKey(to='players.Team'),
        ),
        migrations.AddField(
            model_name='player',
            name='tournament',
            field=models.ForeignKey(to='players.Tournament'),
        ),
        migrations.AddField(
            model_name='match',
            name='home_team',
            field=models.ForeignKey(related_name='home', default=0, to='players.Team'),
        ),
        migrations.AddField(
            model_name='match',
            name='tournament',
            field=models.ForeignKey(to='players.Tournament'),
        ),
        migrations.AddField(
            model_name='match',
            name='visit_team',
            field=models.ForeignKey(related_name='visit', default=0, to='players.Team'),
        ),
    ]
