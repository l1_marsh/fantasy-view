# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('players', '0005_auto_20150807_1159'),
    ]

    operations = [
        migrations.AddField(
            model_name='team',
            name='url_img',
            field=models.CharField(max_length=1024, null=True),
        ),
    ]
