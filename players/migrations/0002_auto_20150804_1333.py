# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('players', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='tournament',
            name='next_tour',
            field=models.IntegerField(null=True),
        ),
        migrations.AlterField(
            model_name='player',
            name='amplua',
            field=models.IntegerField(max_length=1, choices=[(1, 'Вратарь'), (2, 'Защитник'), (4, 'Нападающий'), (3, 'Полузащитник')], default=1, null=True),
        ),
    ]
