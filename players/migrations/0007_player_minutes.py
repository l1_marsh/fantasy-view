# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('players', '0006_team_url_img'),
    ]

    operations = [
        migrations.AddField(
            model_name='player',
            name='minutes',
            field=models.IntegerField(default=0),
        ),
    ]
