# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('players', '0004_auto_20150806_1632'),
    ]

    operations = [
        migrations.AlterField(
            model_name='match',
            name='home_team_goals',
            field=models.IntegerField(max_length=2, null=True),
        ),
        migrations.AlterField(
            model_name='match',
            name='visit_team_goals',
            field=models.IntegerField(max_length=2, null=True),
        ),
    ]
