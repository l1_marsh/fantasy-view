# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('players', '0003_team_fantasypoints'),
    ]

    operations = [
        migrations.AddField(
            model_name='player',
            name='clean_sheets',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='player',
            name='goals',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='player',
            name='passes',
            field=models.IntegerField(default=0),
        ),
    ]
