# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('players', '0002_auto_20150804_1333'),
    ]

    operations = [
        migrations.AddField(
            model_name='team',
            name='fantasypoints',
            field=models.IntegerField(null=True),
        ),
    ]
