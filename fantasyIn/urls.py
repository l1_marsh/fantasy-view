"""fantasyIn URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin
from players import urls as players_urls
from players import views as players_views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    url(r'^$', players_views.home, name='home'), 
    url(r'^(?P<tournament_id>[0-9]+)/$', players_views.tournament_home, name='tournament_home'),
    url(r'^(?P<tournament_id>[0-9]+)/matches/$', players_views.matches, name='matches'),
    url(r'^(?P<tournament_id>[0-9]+)/matches/(?P<match_id>[0-9]+)/$', players_views.match_detail, name='match_detail'),
    url(r'^(?P<tournament_id>[0-9]+)/teams/$', players_views.teams, name='teams'),
    url(r'^(?P<tournament_id>[0-9]+)/teams/(?P<team_id>[0-9]+)/$', players_views.team_detail, name='team_detail'),
    url(r'^(?P<tournament_id>[0-9]+)/players/$', players_views.players, name='players'),
    url(r'^(?P<tournament_id>[0-9]+)/players/(?P<player_id>[0-9]+)/$', players_views.player_detail, name='player_detail'),
    url(r'^(?P<tournament_id>[0-9]+)/results/$', players_views.pmr, name='pmr'),
    url(r'^admin/', include(admin.site.urls)),

] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
